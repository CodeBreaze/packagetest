<?php

namespace Codebrease\Sample\Facades;

use Illuminate\Support\Facades\Facade;

class Message extends Facade
{
	protected static function getFacadeAccessor(){
		return 'codebreazse-sample';
	}
}