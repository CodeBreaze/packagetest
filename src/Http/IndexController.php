<?php

namespace Codebreaze\Sample\Http;

use Illuminate\Routing\Controller as BaseController;
use Sample;

class IndexController extends BaseController
{
    public function getToday()
    {
        return Sample::displaySample() . ' from controller.';
    }
}